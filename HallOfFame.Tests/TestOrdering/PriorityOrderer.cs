﻿using System;
using System.Collections.Generic;
using System.Linq;

using Xunit.Abstractions;
using Xunit.Sdk;

namespace HallOfFame.Tests.TestOrdering
{
    // pasted from http://hamidmosalla.com/2018/08/16/xunit-control-the-test-execution-order/

    /// <summary>
    /// Class that orders test by their priority so the run in intended order
    /// not breaking each other in a tedious to fix way.
    /// </summary>
    public class PriorityOrderer : ITestCaseOrderer
    {
        /// <summary>
        /// Orders test cases by priority.
        /// </summary>
        /// <typeparam name="TTestCase">Test case type.</typeparam>
        /// <param name="testCases">Tests to order.</param>
        /// <returns>Ordered tests.</returns>
        public IEnumerable<TTestCase> OrderTestCases<TTestCase>(IEnumerable<TTestCase> testCases) where TTestCase : ITestCase
        {
            var sortedMethods = new SortedDictionary<int, List<TTestCase>>();

            foreach (TTestCase testCase in testCases)
            {
                int priority = 0;

                foreach (IAttributeInfo attr in testCase.TestMethod.Method.GetCustomAttributes((typeof(TestPriorityAttribute).AssemblyQualifiedName)))
                    priority = attr.GetNamedArgument<int>("Priority");

                GetOrCreate(sortedMethods, priority).Add(testCase);
            }

            foreach (var list in sortedMethods.Keys.Select(priority => sortedMethods[priority]))
            {
                list.Sort((x, y) => StringComparer.OrdinalIgnoreCase.Compare(x.TestMethod.Method.Name, y.TestMethod.Method.Name));
                foreach (TTestCase testCase in list) yield return testCase;

            }
        }

        /// <summary>
        /// Helper method. Gets a value from dictionary by key, or creates new one if
        /// not present.
        /// </summary>
        /// <typeparam name="TKey">Keys of the dictionary.</typeparam>
        /// <typeparam name="TValue">Values of the dictionary.</typeparam>
        /// <param name="dictionary">Dictionary to get or create from.</param>
        /// <param name="key">Key to get or create.</param>
        /// <returns>Value from dictionary, or created one.</returns>
        static TValue GetOrCreate<TKey, TValue>(IDictionary<TKey, TValue> dictionary, TKey key)
            where TValue : new()
        { 
            if (dictionary.TryGetValue(key, out TValue result)) return result;

            result = new TValue();
            dictionary[key] = result;

            return result;
        }
    }
}
