﻿using System;

namespace HallOfFame.Tests.TestOrdering
{
    /// <summary>
    /// Attribute that controls order in which tests are run.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class TestPriorityAttribute : Attribute
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="priority">Desired priority fro a given test method.</param>
        public TestPriorityAttribute(int priority)
        {
            Priority = priority;
        }

        /// <summary>
        /// Test priority. Lower number is higher priority.
        /// </summary>
        public int Priority { get; private set; }
    }
}
