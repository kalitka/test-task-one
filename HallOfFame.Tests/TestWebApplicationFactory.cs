﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace HallOfFame.Tests
{
    /// <summary>
    /// Class that creates web application with mock in-memory database
    /// to run integration tests on.
    /// </summary>
    /// <typeparam name="TStartup">Startup class to use in web app.</typeparam>
    public class TestWebApplicationFactory<TStartup> : WebApplicationFactory<TStartup>
        where TStartup : class
    {
        /// <summary>
        /// Overriden configuration that adds mock database.
        /// </summary>
        /// <param name="builder"><see cref="IWebHostBuilder"/> instance to build.</param>
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                var serviceProvider = new ServiceCollection()
                    .AddEntityFrameworkInMemoryDatabase()
                    .BuildServiceProvider();

                services.AddDbContext<PersonContext>(options =>
                {
                    options.UseInMemoryDatabase("InMemoryTestDB");
                    options.UseInternalServiceProvider(serviceProvider);
                });

                var sp = services.BuildServiceProvider();
                using (var scope = sp.CreateScope())
                {
                    var scopedServices = scope.ServiceProvider;
                    var db = scopedServices.GetRequiredService<PersonContext>();
                    // we don't care about migrations here
                    // (we got grand total of one anyway)
                    db.Database.EnsureCreated();
                    Utilities.PopulateContext(db);
                }
            });
        }
    }
}
