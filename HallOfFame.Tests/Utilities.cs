﻿using System.Collections.Generic;

using HallOfFame.Models;

namespace HallOfFame.Tests
{
    /// <summary>
    /// Helper class to populate mock database with mock people.
    /// </summary>
    public static class Utilities
    {
        /// <summary>
        /// Populates given context with four test values.
        /// </summary>
        /// <param name="context">Context to fill.</param>
        public static void PopulateContext(PersonContext context)
        {
            foreach (var person in GetTestPeople())
            {
                person.PopulateInternalSkills(context);
                context.People.Add(person);
            }
            context.SaveChanges();
        }

        /// <summary>
        /// Used to create list of mock <see cref="Person"/> instances.
        /// </summary>
        /// <returns>Four filled persons.</returns>
        /// <remarks>Ids are set here explicitly, but in actual app this won't work.
        /// </remarks>
        private static IEnumerable<Person> GetTestPeople()
        {

            /* okay, so these fake ids are not updating mock DB's autogeneration
               and PeopleController.GetNextId returns 1 and everything dies trying to
               push a record if we have id 1 here
               that's the reason these ids start from 2 */

            var person1 = new Person()
            {
                Id = 2,
                Name = "test_one",
                DisplayName = "Test person 1"
            };
            person1.Skills.Add(new SkillFacade() { Name = "Test skill 1", Level = 7 });
            yield return person1;

            var person2 = new Person()
            {
                Id = 3,
                Name = "test_two",
                DisplayName = "Test person 2"
            };
            person2.Skills.Add(new SkillFacade() { Name = "Test skill 1", Level = 3 });
            person2.Skills.Add(new SkillFacade() { Name = "Test skill 2", Level = 9 });
            yield return person2;

            var person3 = new Person()
            {
                Id = 4,
                Name = "test_more_than_two_but_less_than_four",
                DisplayName = "Gordon Freeman"
            };
            person3.Skills.Add(new SkillFacade() { Name = "Headcrab bashing", Level = 10 });
            yield return person3;

            var person4 = new Person()
            {
                Id = 5,
                Name = "test_four",
                DisplayName = "Test person 4"
            };
            // no skill, just like me :(
            yield return person4;
        }
    }
}
