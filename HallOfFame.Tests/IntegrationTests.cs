﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Xunit;

using HallOfFame.Models;
using HallOfFame.Tests.TestOrdering;

[assembly: TestCollectionOrderer("HallOfFame.Tests.TestOrdering.PriorityOrderer",
    "HallOfFame.Tests")]
// rip performance
[assembly: CollectionBehavior(DisableTestParallelization = true)]

namespace HallOfFame.Tests
{
    /// <summary>
    /// Integration tests for the Hall of Fame WebAPI.
    /// Literally my first attempt at integration tests ever.
    /// DB is mocked, but logs are not (but who cares?)
    /// </summary>
    public class IntegrationTests
        : IClassFixture<TestWebApplicationFactory<Startup>>
    {
        /// <summary>
        /// <see cref="HttpClient"/> instance to use throughout the tests.
        /// </summary>
        private readonly HttpClient _client;

        /// <summary>
        /// Injected factory with mocked database.
        /// </summary>
        private readonly TestWebApplicationFactory<Startup> _factory;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="factory">Factory to use, delivered via DI magic.</param>
        public IntegrationTests(TestWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
            _client = _factory.CreateClient();
        }

        // "arrange" part is done elsewhere

        /// <summary>
        /// "Get all people" API call test.
        /// </summary>
        /// <returns>A <see cref="Task"/> so the test is awaitable.</returns>
        [Fact, TestPriority(1)]
        public async Task GetAll_PositiveTest()
        {
            // act
            var response = await _client.GetAsync("/api/v1/persons");
            
            // assert (a lot of stuff)
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            var responseBody = await response.Content.ReadAsStringAsync();
            var personList
                = JsonConvert.DeserializeObject<IEnumerable<Person>>(responseBody);
            Assert.Equal(4, personList.Count());

            // check random stuff
            Assert.Equal("Gordon Freeman", personList.ElementAt(2).DisplayName);
            Assert.Equal("test_four", personList.ElementAt(3).Name);
            Assert.Equal(2, personList.ElementAt(0).Id);
            // skill checks
            var skilledPerson = personList.ElementAt(1);
            Assert.Equal(2, skilledPerson.Skills.Count);
            Assert.Equal("Test skill 1", skilledPerson.Skills.ElementAt(0).Name);
            Assert.Equal(9, skilledPerson.Skills.ElementAt(1).Level);
        }

        /// <summary>
        /// "Get specific person by id" API call positive test.
        /// Checks we're have a certain physicist under id 3 in our mock DB.
        /// </summary>
        /// <returns>A <see cref="Task"/> so the test is awaitable.</returns>
        [Fact, TestPriority(1)]
        public async Task GetSpecific_PositiveTest()
        {
            // act
            var response = await _client.GetAsync("/api/v1/person/4");

            // assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var responseBody = await response.Content.ReadAsStringAsync();
            var gordon = JsonConvert.DeserializeObject<Person>(responseBody);
            Assert.Equal(4, gordon.Id);
            Assert.Equal("Gordon Freeman", gordon.DisplayName);
            Assert.Equal("test_more_than_two_but_less_than_four", gordon.Name);
            Assert.Equal(1, gordon.Skills.Count);
            var skill = gordon.Skills.ElementAt(0);
            Assert.Equal(10, skill.Level);
            Assert.Equal("Headcrab bashing", skill.Name);
        }

        /// <summary>
        /// "Get specific person by id" API call negative test.
        /// Checks various wrong ids to return 404 Not found.
        /// </summary>
        /// <param name="wrongId">Wrong (anything outside of 1-4 range) id to test.</param>
        /// <returns>A <see cref="Task"/> so the test is awaitable.</returns>
        [Theory, TestPriority(1)]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(6)]
        [InlineData(-1)]
        [InlineData(1234567890)]
        public async Task GetSpecific_NegativeTest(long wrongId)
        {
            // act
            var response = await _client.GetAsync($"api/v1/person/{wrongId}");
            // assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        /// <summary>
        /// "Delete person by id" API call positive test.
        /// Removes id 4, then _really_ checks it's gone.
        /// </summary>
        /// <returns>A <see cref="Task"/> so the test is awaitable.</returns>
        [Fact, TestPriority(2)]
        public async Task Delete_PositiveTest()
        {
            // act
            var response = await _client.DeleteAsync("/api/v1/person/4");
            // okay i'm going to stop writing these "act" and "assert" comments
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            // let's check EVERYTHING
            var lengthResponse = await _client.GetAsync("/api/v1/persons");
            var peopleListString = await lengthResponse.Content.ReadAsStringAsync();
            var modifiedPeopleList
                = JsonConvert.DeserializeObject<IEnumerable<Person>>(peopleListString);
            Assert.Equal(3, modifiedPeopleList.Count());
            var existenceResponse = await _client.GetAsync("/api/v1/person/4");
            Assert.Equal(HttpStatusCode.NotFound, existenceResponse.StatusCode);
        }

        /// <summary>
        /// "Delete person by id" API call negative test.
        /// Checks various wrong ids to return 404 Not found.
        /// </summary>
        /// <param name="wrongId">Wrong (anything outside of 1-4 range) id to test.</param>
        /// <returns>A <see cref="Task"/> so the test is awaitable.</returns>
        [Theory, TestPriority(2)]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(6)]
        [InlineData(-1)]
        [InlineData(1234567890)]
        public async Task Delete_NegativeTest(long wrongId)
        {
            var response = await _client.DeleteAsync($"/api/v1/person/{wrongId}");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        /// <summary>
        /// "Put person" API call positive test.
        /// Id generation is broken on mock DB, so other people's ids start from 2,
        /// and new person is assigned id 1. This is expected.
        /// </summary>
        /// <returns>A <see cref="Task"/> so the test is awaitable.</returns>
        [Fact, TestPriority(3)]
        public async Task Put_PositiveTest()
        {
            // wow, we have to do some arrangement
            var validPerson = new Person()
            {
                // id is ommited, defaults to null
                Name = "add_test",
                DisplayName = "Runtime added test person"
            };
            validPerson.Skills.Add(new SkillFacade() { Name = "Added skill", Level = 3 });

            var jsonPerson = JsonConvert.SerializeObject(validPerson);
            var stringContent = new StringContent(jsonPerson, Encoding.UTF8, "application/json");
            var response = await _client.PutAsync("/api/v1/person", stringContent);

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var personResponse = await _client.GetAsync("/api/v1/person/1");
            var personResponseContent = await personResponse.Content.ReadAsStringAsync();
            var personToCheck = JsonConvert.DeserializeObject<Person>(personResponseContent);
            // id generation is broken on mock DB
            Assert.Equal(1, personToCheck.Id);
            Assert.Equal("add_test", personToCheck.Name);
            Assert.Equal("Runtime added test person", personToCheck.DisplayName);
            Assert.Equal(1, personToCheck.Skills.Count);
            var skill = personToCheck.Skills.ElementAt(0);
            Assert.Equal(3, skill.Level);
            Assert.Equal("Added skill", skill.Name);
        }

        /// <summary>
        /// "Put person" API call negative test.
        /// Checks that malformed persons can't be put into the DB.
        /// </summary>
        /// <returns>A <see cref="Task"/> so the test is awaitable.</returns>
        [Fact, TestPriority(3)]
        public async Task Put_NegativeTest()
        {
            // copy-paste programming is bad for your mental health

            // case one: id set explicitly
            var personWithIdSet = new Person()
            {
                Id = 78,
                Name = "set_id_test",
                DisplayName = "I AM ERROR"
            };
            personWithIdSet.Skills.Add(new SkillFacade() { Name = "PogChamp spam", Level = 2 });

            var jsonPerson = JsonConvert.SerializeObject(personWithIdSet);
            var stringContent = new StringContent(jsonPerson, Encoding.UTF8, "application/json");
            var response = await _client.PutAsync("/api/v1/person", stringContent);

            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);

            // case two: invalid skill level
            var personWithBadLevel = new Person()
            {
                Name = "bad_level_test",
                DisplayName = "I AM ERROR"
            };
            personWithBadLevel.Skills.Add(new SkillFacade() { Name = "PogChamp spam", Level = 11 });

            jsonPerson = JsonConvert.SerializeObject(personWithBadLevel);
            stringContent = new StringContent(jsonPerson, Encoding.UTF8, "application/json");
            response = await _client.PutAsync("/api/v1/person", stringContent);

            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);

            // case three: missing name field
            var personWithNoName = new Person()
            {
                DisplayName = "I AM ERROR"
            };
            personWithNoName.Skills.Add(new SkillFacade() { Name = "PogChamp spam", Level = 2 });

            jsonPerson = JsonConvert.SerializeObject(personWithNoName);
            stringContent = new StringContent(jsonPerson, Encoding.UTF8, "application/json");
            response = await _client.PutAsync("/api/v1/person", stringContent);

            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        /// <summary>
        /// "Update person by id" API call positive test.
        /// Checks that people in DB are indeed updateable.
        /// </summary>
        /// <returns>A <see cref="Task"/> so the test is awaitable.</returns>
        [Fact, TestPriority(4)]
        public async Task Post_PositiveTest()
        {
            var replacementPerson = new Person()
            {
                // id's still ommited
                Name = "update_test",
                DisplayName = "Runtime changed test person"
            };
            // this one updates exising skill
            replacementPerson.Skills.Add(new SkillFacade() { Name = "Test skill 1", Level = 10 });
            replacementPerson.Skills.Add(new SkillFacade() { Name = "Recently added skill", Level = 1 });

            var jsonPerson = JsonConvert.SerializeObject(replacementPerson);
            var stringContent = new StringContent(jsonPerson, Encoding.UTF8, "application/json");
            var response = await _client.PostAsync("/api/v1/person/3", stringContent);

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            // let's recheck this
            var assuringResponse = await _client.GetAsync("/api/v1/person/3");
            var responseBody = await assuringResponse.Content.ReadAsStringAsync();
            var personToCheck = JsonConvert.DeserializeObject<Person>(responseBody);

            Assert.Equal("update_test", personToCheck.Name);
            Assert.Equal("Runtime changed test person", personToCheck.DisplayName);
            Assert.Equal(2, personToCheck.Skills.Count);
            var testSkill1 = personToCheck.Skills.ElementAt(0);
            Assert.Equal("Test skill 1", testSkill1.Name);
            Assert.Equal(10, testSkill1.Level);
            var recentSkill = personToCheck.Skills.ElementAt(1);
            Assert.Equal("Recently added skill", recentSkill.Name);
            Assert.Equal(1, recentSkill.Level);
        }

        /// <summary>
        /// "Update person by id" API call negative test.
        /// Checks for proper error responses on malformed requests.
        /// </summary>
        /// <returns>A <see cref="Task"/> so the test is awaitable.</returns>
        [Fact, TestPriority(4)]
        public async Task Post_NegativeTest()
        {
            // case one: invalid id in url
            var replacementPerson = new Person()
            {
                // id's still ommited
                Name = "update_test_wrong_id",
                DisplayName = "I AM ERROR"
            };
            var jsonPerson = JsonConvert.SerializeObject(replacementPerson);
            var stringContent = new StringContent(jsonPerson, Encoding.UTF8, "application/json");
            var response = await _client.PostAsync("/api/v1/person/110", stringContent);

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);

            // case two: id set in replacement person
            var personWithIdSet = new Person()
            {
                Id = 78,
                Name = "set_id_test",
                DisplayName = "I AM ERROR"
            };
            personWithIdSet.Skills.Add(new SkillFacade() { Name = "PogChamp spam", Level = 2 });

            jsonPerson = JsonConvert.SerializeObject(personWithIdSet);
            stringContent = new StringContent(jsonPerson, Encoding.UTF8, "application/json");
            response = await _client.PostAsync("/api/v1/person/4", stringContent);

            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);

            // case three: invalid skill level
            var personWithBadLevel = new Person()
            {
                Name = "bad_level_test",
                DisplayName = "I AM ERROR"
            };
            personWithBadLevel.Skills.Add(new SkillFacade() { Name = "PogChamp spam", Level = 11 });

            jsonPerson = JsonConvert.SerializeObject(personWithBadLevel);
            stringContent = new StringContent(jsonPerson, Encoding.UTF8, "application/json");
            response = await _client.PostAsync("/api/v1/person/4", stringContent);

            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);

            // case four: missing DisplayName
            var personWithNoName = new Person()
            {
                Name = "I AM ERROR"
            };
            personWithNoName.Skills.Add(new SkillFacade() { Name = "PogChamp spam", Level = 2 });

            jsonPerson = JsonConvert.SerializeObject(personWithNoName);
            stringContent = new StringContent(jsonPerson, Encoding.UTF8, "application/json");
            response = await _client.PostAsync("/api/v1/person/4", stringContent);

            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
