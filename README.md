# Тестовое задание
tl;dr — игрушечная БД условных сотрудников с условными навыками на ASP.NET Core
и EF Core, общающаяся с внешним миром через WebAPI. Еще должны быть прикручены
серьезные вещи серьезного бизнеса, вроде юнит/интеграционных тестов, логов и 
браузерной версии апи через Swagger.  

## Описание
Есть две сущности — люди и навыки:  
**Person**  
```
{
	id: long,
	name: string,
	displayName: string,
	skills: [Skill, Skill, Skill, ...]
}
```  
и **Skill**
```
{
	name: string,
	level: byte // 1-10
}
```  
и нужно сделать БД с привычными CRUD операциями на них, методы:
* GET /api/v1/persons — список всех
* GET /api/v1/person/{id} — конкретный человек, если такого нет — 404
* PUT /api/v1/person, в теле запроса Person — помещение в БД, id должен быть `null`, иначе — 400
* POST /api/v1/person/{id}, в теле запроса Person — обновление по id, у Person из запроса аналогично пустой id
* DELETE /api/v1/person/{id} — удаление по id, если никого и так нет — 404  

Код написан по уже надоевшему мне [RSDN](http://rsdn.org/article/mag/200401/codestyle.XML),
только с ограничением в 90 символов на строку
(потому что в 1920×1080 в две колонки на 90% идеально входит).  
Ну посмотрим, что из этого вышло.

## Технические заметки
Вместо БД — MS SQL Server Express 2017, для работы нужна база "people",
доступная текущей учеткe Windows. Таблицы можно создать миграцией "Init".
### Велосипеды
Для работы по спецификации пришлось ~~написать~~ найти:
* [ILogger, пишущий в файл](https://github.com/nreco/logging), потому что из
коробки такого нет
* [Middleware для отлова и логирования ошибок](https://stackoverflow.com/a/38935583)
* [Сортировщик тестов по задаваемому приоритету](http://hamidmosalla.com/2018/08/16/xunit-control-the-test-execution-order/),
чтобы тесты на одной БД запускались в заданном порядке и не ломали друг друга

## Известный провал
Юнит-тестов нет. Я слишком поздно осознал, что контроллер использует `PersonContext`
напрямую (как было сгенерировано VS) вместо выноса его за абстракцию в виде 
сервиса через какой-нибудь `IDbService`, что позволило бы сделать мок БД 
написанием еще одной реализации — списка с оберткой, а не подменой конфигурации
всего приложения, как это сделано в интеграционных.  
Хотя в таком простом приложении интеграционные и юнит-тесты мало чем отличаются.
Например, есть интеграционный тест (`IntegrationTests.cs:86`):
```csharp
        public async Task GetSpecific_PositiveTest()
        {
            // act
            var response = await _client.GetAsync("/api/v1/person/4");

            // assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            var responseBody = await response.Content.ReadAsStringAsync();
            var gordon = JsonConvert.DeserializeObject<Person>(responseBody);
            Assert.Equal(4, gordon.Id);
            Assert.Equal("Gordon Freeman", gordon.DisplayName);
            Assert.Equal("test_more_than_two_but_less_than_four", gordon.Name);
            Assert.Equal(1, gordon.Skills.Count);
            var skill = gordon.Skills.ElementAt(0);
            Assert.Equal(10, skill.Level);
            Assert.Equal("Headcrab bashing", skill.Name);
        }
```
юнит-тест этого же метода выглядел бы примерно так:
 ```csharp
        public async Task GetSpecific_PositiveTest()
        {
            // act
            var response = await _controller.GetPerson(4);

            // assert
            Assert.Equal(4, response.Id);
            Assert.Equal("Gordon Freeman", response.DisplayName);
            Assert.Equal("test_more_than_two_but_less_than_four", response.Name);
            Assert.Equal(1, response.Skills.Count);
            var skill = response.Skills.ElementAt(0);
            Assert.Equal(10, skill.Level);
            Assert.Equal("Headcrab bashing", skill.Name);
        }
```
то есть ровно также, за вычетом кода, связанного с запросами
## Выводы
* Брать больше времени на задания
* **Брать больше времени на задания**
* Думать про архитектуру и тестируемость кода
* Писать тесты до или вместе с кодом, а не после
* Не доверять браузерным редакторам Markdown