﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace HallOfFame.Models
{
    /// <summary>
    /// Class that represents a named skill with a value from WebAPI's point of view.
    /// Consists of skill name and skill level.
    /// </summary>
    public class SkillFacade
    {
        /// <summary>
        /// Name of the skill.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Skill level. Must be in range from 1 to 10 inclusive.
        /// </summary>
        [Required, Range(1, 10)]
        public byte Level { get; set; }

        /// <summary>
        /// Factory method to extract data from <see cref="PersonSkill"/> instance.
        /// </summary>
        /// <param name="personSkill">Instance to get name and level from.</param>
        /// <returns>New <see cref="SkillFacade"/> with data from
        /// <paramref name="personSkill"/></returns>
        public static SkillFacade FromPersonSkill(PersonSkill personSkill)
        {
            if (personSkill == null)
            {
                throw new ArgumentNullException(nameof(personSkill));
            }
            return new SkillFacade()
            {
                Name = personSkill.Skill.Name,
                Level = personSkill.Level
            };
        }

        /// <summary>
        /// Gets a skill with the same name from given context. If not present,
        /// creates one.
        /// </summary>
        /// <param name="context">Context to check for existing entry.</param>
        /// <returns>Id of related skill in given context.</returns>
        public long ToSkillId(PersonContext context)
        {
            Skill skill;
            try
            {
                skill = context.Skills.SingleOrDefault(s => s.Name.Equals(Name));
            }
            catch (InvalidOperationException)
            {
                // this should never happen, we enforce uniqueness of names many times
                // but still, explode violently
                throw;
            }
            if (skill == null)
            {
                // skill with this name not yet persent,
                // let's add one and return its generated id
                var added = context.Skills.Add(new Skill() { Name = Name });
                context.SaveChanges();
                return added.Entity.Id;
            }
            return skill.Id;
        }
    }
}
