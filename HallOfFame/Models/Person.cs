﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace HallOfFame.Models
{
    /// <summary>
    /// Class that represents a person in API, produced and comsumed there.
    /// </summary>
    public class Person
    {
        /// <summary>
        /// Primary key. Nullable here because specs require to supply null values
        /// when inserting/updating entries. Lots of quirks are used to make sure
        /// these nulls won't be shipped to the DB.
        /// </summary>
        public long? Id { get; set; }

        // no idea why two name fields
        // also no validation whatsoever apart from existence check

        /// <summary>
        /// One of the fields to display a person's name, I guess?
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// One of the fields to display a person's name, I guess?
        /// </summary>
        [Required]
        public string DisplayName { get; set; }

        /// <summary>
        /// List of person's skills from WebAPI point of view. Generated from DB side
        /// on produced Persons, used to fill DB's side on consumed Persons.
        /// </summary>
        [NotMapped, Required]
        public ICollection<SkillFacade> Skills { get; private set; }
            = new List<SkillFacade>();

        /// <summary>
        /// List of skills from DB's point of view. Not included in JSON output.
        /// </summary>
        [Newtonsoft.Json.JsonIgnore, BindNever]
        private ICollection<PersonSkill> InternalSkills { get; set; }
            = new List<PersonSkill>();

        /// <summary>
        /// Updates <see cref="InternalSkills"/> from <see cref="Skills"/> data.
        /// Should be used on instances received from WebAPI to make them DB-friendly.
        /// </summary>
        /// <param name="context">Database context to get skill ids from.</param>
        public void PopulateInternalSkills(PersonContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }
            InternalSkills.Clear();
            foreach (var skillFacade in Skills)
            {
                var toAdd = new PersonSkill()
                {
                    PersonId = Id.Value,
                    SkillId = skillFacade.ToSkillId(context),
                    Level = skillFacade.Level
                };
                InternalSkills.Add(toAdd);
            }
        }

        /// <summary>
        /// Updates <see cref="Skills"/> with either another list of skills (used to
        /// update record in-place) or data from <see cref="InternalSkills"/> to make
        /// person loaded from DB WebAPI-output-in-JSON-friendly.
        /// </summary>
        /// <param name="from">Optional list of skills to replace current.
        /// If ommited, data will be updated to match <see cref="InternalSkills"/></param>
        public void PopulateSkills(IEnumerable<SkillFacade> from = null)
        {
            Skills.Clear();
            var source = from ??
                InternalSkills.Select(ps => SkillFacade.FromPersonSkill(ps));
            foreach (var skillFacade in source)
            {
                Skills.Add(skillFacade);
            }
        }

        /// <summary>
        /// Updates data properties (but not the key) from another instance.
        /// </summary>
        /// <param name="otherPerson">Person to get new values from.</param>
        public void UpdateDataFrom(Person otherPerson)
        { 
            if (otherPerson == null)
            {
                throw new ArgumentNullException(nameof(otherPerson));
            }
            Name = otherPerson.Name;
            DisplayName = otherPerson.DisplayName;
            // this is not perfect
            Skills = new List<SkillFacade>(otherPerson.Skills);
            InternalSkills = new List<PersonSkill>(otherPerson.InternalSkills);
        }
    }
}
