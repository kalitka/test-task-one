﻿namespace HallOfFame.Models
{
    /// <summary>
    /// Class that represents join entity pairing <see cref="Person"/> instances with
    /// their <see cref="Skill"/>s from DB's point of view.
    /// </summary>
    public class PersonSkill
    {
        /// <summary>
        /// Id of skill that is in this pairing.
        /// </summary>
        public long SkillId { get; set; }

        /// <summary>
        /// Reference to actual skill from this pairing.
        /// </summary>
        public Skill Skill { get; private set; }

        /// <summary>
        /// Id of person that is in this pairing.
        /// </summary>
        public long PersonId { get; set; }

        /// <summary>
        /// Reference to actual person instance.
        /// </summary>
        public Person Person { get; private set; }

        /// <summary>
        /// Level of the skill that the person has. This value is actually stored
        /// in the DB.
        /// </summary>
        public byte Level { get; set; }
    }
}
