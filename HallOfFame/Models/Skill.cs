﻿namespace HallOfFame.Models
{
    /// <summary>
    /// Class that represents a named skill from DB's point of view.
    /// Simple entity consisting of unique identifier and an also unique name.
    /// </summary>
    public class Skill
    {
        /// <summary>
        /// Primary key property.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Skill name. Uniqueness is guaranteed via index in DbContext.
        /// </summary>
        public string Name { get; set; }
    }
}
