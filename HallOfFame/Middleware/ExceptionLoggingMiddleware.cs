﻿using System;
using System.Net;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace HallOfFame.Middleware
{

    // all credit goes to https://stackoverflow.com/a/38935583
    // i only slightly tweaked it

    /// <summary>
    /// Middleware for logging otherwise uncaught exceptions to log file. Also returns
    /// plain 500 Internal server error responses on mentioned exceptions.
    /// </summary>
    public class ExceptionLoggingMiddleware
    {
        /// <summary>
        /// Probably the next middleware in the pipeline to process the request.
        /// </summary>
        private readonly RequestDelegate _next;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="next">Required to make this work.</param>
        public ExceptionLoggingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        /// <summary>
        /// Invokes this middleware. Requests pass through unless there's an exception
        /// to be logged.
        /// </summary>
        /// <param name="context">Context to use.</param>
        /// <param name="logger">Used logger instance.</param>
        /// <returns>Task with processing result of this middleware (?).</returns>
        public async Task Invoke(HttpContext context,
            ILogger<ExceptionLoggingMiddleware> logger)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex, logger);
            }
        }

        /// <summary>
        /// Logs the exceptions and returns plain 500 error code to client.
        /// </summary>
        /// <param name="context">Context to use.</param>
        /// <param name="exception">Exception to log.</param>
        /// <param name="logger">Logger to write to logs with.</param>
        /// <returns>Resulting response asynchronously (?).</returns>
        private static Task HandleExceptionAsync(HttpContext context,
            Exception exception, ILogger<ExceptionLoggingMiddleware> logger)
        {
            logger.LogError(exception, "Something went wrong, here's the exception "
                + "that caused this mess:\n");
            var code = HttpStatusCode.InternalServerError; // 500 if unexpected
            context.Response.StatusCode = (int)code;
            return context.Response.WriteAsync(String.Empty);
        }
    }
}
