﻿using Microsoft.EntityFrameworkCore;

using HallOfFame.Models;

namespace HallOfFame
{
    /// <summary>
    /// Database context used throughout the app.
    /// </summary>
    public class PersonContext : DbContext
    {
        /// <summary>
        /// <see cref="Person"/> collection to manipulate from the controller.
        /// </summary>
        public DbSet<Person> People { get; private set; }

        /// <summary>
        /// <see cref="Skill"/> collection that is used a few times in data conversion.
        /// </summary>
        public DbSet<Skill> Skills { get; private set; }

        /// <summary>
        /// Constructor that does nothing yet still required for autogeneration to work.
        /// </summary>
        /// <param name="options">Context options.</param>
        public PersonContext(DbContextOptions<PersonContext> options)
            : base(options) { }

        /// <summary>
        /// Configures few things not set directly in <see cref="Models"/> POCOs.
        /// </summary>
        /// <param name="modelBuilder">Instance used to configure.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // setting up PersonSkill's composite primary key
            modelBuilder.Entity<PersonSkill>()
                .HasKey(ps => new { ps.PersonId, ps.SkillId });
            // setting up InternalSkills property as it's private and won't be
            // picked up by EF Core automagically.
            modelBuilder.Entity<PersonSkill>()
                .HasOne(ps => ps.Person)
                .WithMany("InternalSkills");
            // index to ensure skill names are unique
            modelBuilder.Entity<Skill>()
                .HasIndex(s => s.Name).IsUnique();

        }
    }
}
