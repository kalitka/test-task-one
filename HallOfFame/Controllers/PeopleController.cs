﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using HallOfFame.Models;

namespace HallOfFame.Controllers
{
    /// <summary>
    /// Controller class that actually powers this app.
    /// </summary>
    [ApiController]
    public class PeopleController : ControllerBase
    {
        // this thing is only here because of api/v1/personS get-all API method
        /// <summary>
        /// Common beginning part of path to the API endpoints.
        /// </summary>
        private const string CommonPrefix = "api/v1/person";

        /// <summary>
        /// Used database context.
        /// </summary>
        private readonly PersonContext _context;

        /// <summary>
        /// Used logger instance.
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// Constructor with dependency injections.
        /// </summary>
        /// <param name="context">Database context to use.</param>
        /// <param name="logger">Logger to use.</param>
        public PeopleController(PersonContext context, ILogger<PeopleController> logger)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        // GET: api/v1/persons
        /// <summary>
        /// Method to get entire database of <see cref="Person"/>s as one array.
        /// </summary>
        /// <returns>Enumerable of <see cref="Person"/>s from the <see cref="_context"/>
        /// May be empty.</returns>
        [HttpGet(CommonPrefix + "s")]
        public async Task<ActionResult<IEnumerable<Person>>> GetPeople()
        {
            // ObjectMaterializing event didn't make the cut for EF Core AFAIK :(
            // so here we go
            var ret = await _context.People.Include("InternalSkills.Skill")
                .ToListAsync();
            ret.ForEach(p => p.PopulateSkills());
            return ret;
        }

        // GET: api/v1/person/5
        /// <summary>
        /// Method to get one <see cref="Person"/> from <see cref="_context"/> via
        /// unique id.
        /// </summary>
        /// <param name="id">Id to search for.</param>
        /// <returns>Person with corresponding id of 404 Not found.</returns>
        [HttpGet(CommonPrefix + "/{id}")]
        public async Task<ActionResult<Person>> GetPerson(long id)
        {
            // rip performance, but that's the only quick and dirty way to eagerly load
            // related data I could think of right now
            var person = await _context.People.Include("InternalSkills.Skill")
                .SingleOrDefaultAsync(p => p.Id == id);
            if (person == null)
            {
                return LogNotFound(id, "get");
            }
            person.PopulateSkills();
            return person;
        }

        // POST: api/v1/person/5
        /// <summary>
        /// Updates an existing record with provided data.
        /// </summary>
        /// <param name="id">Id of existing person in the database to update.</param>
        /// <param name="person">New data to update with. Its id ignored.</param>
        /// <returns>200 Ok, hopefully. Also 404 Not found if provided
        /// <paramref name="id"/> does not match any database entries.</returns>
        [HttpPost(CommonPrefix + "/{id}")]
        public async Task<IActionResult> PostPerson(long id, Person person)
        {
            if (!ModelState.IsValid || person.Id != null)
            {
                return LogBadRequest();
            }
            var entry = await _context.People.Include("InternalSkills.Skill")
                .SingleOrDefaultAsync(p => p.Id == id);
            if (entry == null)
            {
                return LogNotFound(id, "update");
            }
            person.Id = id;
            person.PopulateInternalSkills(_context);
            entry.UpdateDataFrom(person);
            _context.Entry(entry).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PersonExists(id))
                {
                    _logger.LogWarning("DbUpdateConcurrencyException while updating "
                        + $"id {id}. Returning Not found.");
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok();
        }

        // PUT: api/v1/person
        /// <summary>
        /// Creates a new Person and related records in the database.
        /// </summary>
        /// <param name="person">Data to insert. Id is ignored and replaced
        /// with DB's autoincremented one.</param>
        /// <returns>200 Ok, hopefully. 400 Bad request on malformed data.</returns>
        [HttpPut(CommonPrefix)]
        public async Task<ActionResult<Person>> PutPerson(Person person)
        {
            if (!ModelState.IsValid || person.Id != null)
            {
                return LogBadRequest();
            }
            // this requires two queries to the DB
            // pepeHands
            // first query is to save dummy data to get next valid id
            var nextId = await GetNextId();
            person.Id = nextId;
            // then we use it to generate related entites for other tables
            person.PopulateInternalSkills(_context);
            // and the we save actual info in another query
            var entry = await _context.People.FindAsync(nextId);
            entry.UpdateDataFrom(person);
            _context.Entry(entry).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return Ok();
        }

        // DELETE: api/v1/person/5
        /// <summary>
        /// Deletes existing record by id.
        /// </summary>
        /// <param name="id">Id of person to delete.</param>
        /// <returns>200 Ok if deleted successfully, or 404 Not found if person
        /// with provided id already do not exist.</returns>
        [HttpDelete(CommonPrefix + "/{id}")]
        public async Task<ActionResult<Person>> DeletePerson(long id)
        {
            var person = await _context.People.FindAsync(id);
            if (person == null)
            {
                return LogNotFound(id, "delete");
            }
            _context.People.Remove(person);
            await _context.SaveChangesAsync();
            return Ok();
        }

        /// <summary>
        /// Helper method to check whether <see cref="Person"/> with provided id
        /// exists in current database context.
        /// </summary>
        /// <param name="id">Id to check.</param>
        /// <returns>True if record exists, false otherwise.</returns>
        private bool PersonExists(long id)
        {
            return _context.People.Any(e => e.Id == id);
        }

        /// <summary>
        /// Method to get next valid id value for person. This value should be reused
        /// to save new one after its id-requiring setup.
        /// </summary>
        /// <returns>Fresly-made id value from the database.</returns>
        private async Task<long> GetNextId()
        {
            var testPerson = new Person() { Name = "whatever", DisplayName = "whatnot" };
            var added = _context.Add(testPerson);
            await _context.SaveChangesAsync();
            return added.Entity.Id.Value;
        }

        /// <summary>
        /// Used to return Not found responses while logging them.
        /// </summary>
        /// <param name="id">Id that is missing from DB, but was accessed.</param>
        /// <param name="verb">Supposed action with the id to put into the log.</param>
        /// <returns>Regular <see cref="NotFoundResult"/> to be returned to the client.
        /// </returns>
        private ActionResult LogNotFound(long id, string verb)
        {
            _logger.LogWarning($"User requested to {verb} Person with id {id} "
                + "not found in the database.");
            return NotFound();
        }

        /// <summary>
        /// Used to return Bad request responses while logging them.
        /// </summary>
        /// <returns>Regular <see cref="BadRequestResult"/> to be returned to the client.
        /// </returns>
        private ActionResult LogBadRequest()
        {
            _logger.LogWarning("Got malformed person as input, returning Bad request.");
            return BadRequest();
        }
    }
}
